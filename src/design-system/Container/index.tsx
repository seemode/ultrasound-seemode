import React from 'react';
import cnames from 'classnames';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';

const Container = ({
  children,
  size,
  background,
  fluid,
  className, // eslint-disable-line
  style, // eslint-disable-line
  ...props
}:IProps) => (
  <Box display="flex" justifyContent="center" style={{ background: background }}>
    <Box
      className={cnames(
        styles.container,
        {
          [styles.xxxsmall]: size === 'xxxs',
          [styles.xxsmall]: size === 'xxs',
          [styles.xsmall]: size === 'xs',
          [styles.small]: size === 's',
          [styles.medium]: size === 'm',
          [styles.large]: size === 'l',
          [styles.xlarge]: size === 'xl',
          [styles.fluid]: fluid,
        },
      )}
      {...props}
    >
      { children }
    </Box>
  </Box>
);

Container.defaultProps = {
  styles,
  color: 'transparent',
  size: 'l',
  fluid: false,
  tracking: null,
};

export default Container;
