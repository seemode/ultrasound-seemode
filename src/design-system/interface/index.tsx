import { features } from 'process';
import React from 'react';

interface IProps {
  padding:any,
  paddingTop?:any,
  paddingBottom?:any,
  paddingRight?:any,
  paddingLeft?:any,
  paddingX?:any,
  paddingY?:any,
  alignItems?:any,
  justifyContent?:any,
  spacing?:any,
  direction?:any,
  action?:any
  actions?:any
  variant?:any
  field?:any
  form?:any
  condensed?:any
  columns?:any
  iconBefore?:any
  trigger?:any
  className?: any;
  style?:any;
  children?:any;
  flex?:any;
  innerRef?:any
  size?:any;
  ratio?:any;
  roundedCorners?:any
  width?:number
  height?:number
  dangerouslySetInnerHTML?:any
  intent?:any
  subtle?:any
  styles?:any
  elevation?:any
  fluid?:any
  transparent?:any,
  class?:any
  background?:any
  margin?:any
  marginTop?:any
  marginLeft?:any,
  marginRight?:any,
  marginBottom?:any,
  marginX?:any,
  marginY?:any,
  color?:any,
    // any other props that come into the component
  }

  export default IProps;