import React, { forwardRef } from 'react';
import ReachTooltip from "@reach/tooltip";
import "@reach/tooltip/styles.css";
import Text from '../Text';
import styles from './styles.module.scss';

const Tooltip = forwardRef(({ children, ...props }, ref) => {
  return (
    <ReachTooltip label ="Tooltip-Label" {...props} className={styles.tooltip}>
      <Text type="caption">{children}</Text>
    </ReachTooltip>
  )
});

export default Tooltip;
