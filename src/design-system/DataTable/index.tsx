/* eslint-disable react/require-default-props */
import React, { forwardRef } from 'react';
import ReactDataTable, { createTheme } from 'react-data-table-component';

createTheme('seeMode', {
  text: {
    primary: 'var(--ds-color-text)',
    secondary: 'var(--ds-color-text-muted)',
  },
  highlightOnHover: {
    default: 'var(--ds-color-text)',
    text: 'var(--ds-color-text)',
  },
  sortFocus: {
    default: 'var(--ds-color-text)',
  },
  selected: {
    default: 'var(--ds-color-text)',
    text: 'var(--ds-color-text)',
  },
  background: {
    default: 'transparent',
  },
  context: {
    background: 'transparent',
    text: 'var(--ds-color-text)',
  },
  divider: {
    default: 'var(--ds-color-border-weak)',
  },
  action: {
    button: 'rgba(0,0,0,.54)',
    hover: 'rgba(0,0,0,.08)',
    disabled: 'rgba(0,0,0,.12)',
  },
});

const customStyles = {
  table: {
    style: {
      fontSize: 'var(--ds-font-size-body)',
    }
  },
  rows: {
    style: {
      minHeight: 'var(--ds-size-dimension-xxxl)',
    }
  },
  headCells: {
    style: {
      paddingLeft: 'var(--ds-spacing-s)',
      paddingRight: 'var(--ds-spacing-s)',
      fontSize: 'var(--ds-font-size-display5)',
      fontWeight: 'var(--ds-font-weight-bold)',
    },
  },
  cells: {
    style: {
      fontSize: 'var(--ds-font-size-body)',
      paddingLeft: 'var(--ds-spacing-s)',
      paddingRight: 'var(--ds-spacing-s)',
    },
  },
};


const DataTable = forwardRef((ref:any,props:any) => {
  return (
    <ReactDataTable
      theme="seeMode"
      customStyles={customStyles}
      ref={ref} 
      {...props}
    />
  );
}) ;

export default DataTable;
