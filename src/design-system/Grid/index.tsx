import React from 'react';
import cnames from 'classnames';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';

const Grid = ({ children, columns, condensed }:IProps) => {

  const classnames = cnames(styles.grid, {
    [styles.condensed]: condensed,
  });

  if (!children) return null;

  return (
    <Box
      className={classnames}
      style={{ gridTemplateColumns: `repeat(${columns}, minmax(0, 1fr))` }}
    >
      {children}
    </Box>
  );
};

Grid.defaultProps = {
  columns: 4,
};

export default Grid;
