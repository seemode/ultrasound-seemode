import React, { forwardRef } from 'react';
import cnames from 'classnames';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';

const InteractiveBox = forwardRef((props :any, ref) => {
  const {
    children,
    transparent,
    pill,
    className,
    isAnimated,
    active,
    ...rest
  }  = props ;

  const outerClassnames = cnames(styles.outer, {
    [styles.transparent]: transparent,
    [styles.pill]: pill,
    [styles.notAnimated]: !isAnimated,
    [styles.active]: active,
  });

  const innerClassnames = cnames(styles.inner, className);

  return (
    <Box {...rest} className={outerClassnames} ref={ref}>
      <Box className={innerClassnames}>
        {children}
      </Box>
    </Box>
  );
});

InteractiveBox.defaultProps = {
  className: '',
  pill: null,
  isAnimated: true,
  transparent: null,
  display: 'inline-block'
};

export default InteractiveBox;
