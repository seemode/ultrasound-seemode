import React, { forwardRef } from 'react';
import cnames from 'classnames';
import {
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import "@reach/menu-button/styles.css";
import Box from '../Box';
import Card from '../Card';
import Text from '../Text';
import styles from './styles.module.scss';
import IProps from '../interface';

const Trigger = forwardRef(({ trigger, ...props }:IProps, ref) => {
  return <>{ trigger(props, ref) }</>;
});

const Dropdown = ({ trigger, actions, children, size, ...props }:IProps) => {

  const classNames = cnames({
    [styles.large]: size === 'large',
    [styles.medium]: size === 'medium',
    [styles.small]: size === 'small',
  })

  return (    
      <Menu>
        <MenuButton>Button</MenuButton>
        <MenuPopover className={classNames}>
          <Box marginY="xs">
            <Card
              elevation="medium"
              padding="none"
              fluid
            />
              <MenuItems
                as={Box}
                paddingY="xs"
                className={styles.dropdownItems}
              >
                {children}
              </MenuItems>
         
          </Box>
        </MenuPopover>
      </Menu>
  )
};

const DropdownItem = ({ action, children, iconBefore, ...rest }:IProps) => {
  return (
    <MenuItem
    onSelect={() => {}}
      {...rest}
      className={styles.dropdownItem}
     
    >
      <Item padding='none' children={children} iconBefore={iconBefore} />
    </MenuItem>
  )
} ;

const DropdownLink = ({ action, children, iconBefore, ...rest }:IProps) => {
  return (
    <MenuLink
      {...rest}
      className={styles.dropdownItem}
      
    >
    <Item padding='none' children={children} iconBefore={iconBefore} />
    </MenuLink>
  )
};

const Item = ({ children, iconBefore }:IProps ) => {
  return (
    <Box
      padding="s"
      paddingX="xl"
      display="flex"
      alignItems="center"
    >
      {iconBefore && <Box marginRight="m">{ iconBefore }</Box>}
      <Text>{children}</Text>
    </Box>
  )
}

export {
  DropdownItem,
  DropdownLink,
};

export default Dropdown;
