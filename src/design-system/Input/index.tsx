import React from 'react';
import cnames from 'classnames';
import InteractiveBox from '../InteractiveBox';
import styles from './styles.module.scss';
import IProps from '../interface';

const Input = ({
  field,
  form,
  fluid,
  size,
  ...props
}:IProps) => {

  const classNames = cnames(styles.input, {
    [styles.fluid]: fluid,
    [styles.large]: size === 'large',
  })
  return (
    <InteractiveBox display="block">
      <input element="input" {...field} {...props} className={classNames} />
    </InteractiveBox>
  );
};

export default Input;
