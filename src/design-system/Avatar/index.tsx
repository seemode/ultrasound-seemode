import React, { forwardRef, memo } from 'react';
import cnames from 'classnames';
import Image from '../Image';
import Text from '../Text';

import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';
const Avatar = (forwardRef((props:any, ref) => {
  const {
    loading,
    fluid,
    size,
    className, // Exclude from ...rest
    style, // Exclude from ...rest
    name,
    color,
    src,
   ...rest
    
  } = props;

  return (
    <Box    
      className={
        cnames(
          styles.avatar,
          {
            [styles.loading]: loading,
            [styles[size]]: !!size,
            [styles.fluid]: fluid,
          },
        )
      }
      display="flex"
      alignItems="center"
      justifyContent="center"
      style={color ? { background: color } : null}
      ref={ref}
    >
      {
        src ?
          <Image
            round
            role="presentation"
            ratio="square"
            fluid={fluid}
            {...rest}
          />
        : <Text type="display5">{ name }</Text>
      }
    </Box>
  );
}));


Avatar.defaultProps =  {
  bordered: false,
  fluid: false,
  size: 'medium',
  loadOnVisible: false,
  loading: false,
  srcSet: '',
  src: null,
}  ;

Avatar.displayName = 'Avatar';

export default Avatar;
