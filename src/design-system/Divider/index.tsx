import React from 'react';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';
const Divider = ({
  // Box props
  margin,
  marginTop,
  marginLeft,
  marginRight,
  marginBottom,
  marginX,
  marginY,
  color,
}:IProps) => {

  return (
    <Box
      {...({
        margin,
        marginTop,
        marginLeft,
        marginRight,
        marginBottom,
        marginX,
        marginY,
      })}
      className={styles.divider}
      style={{ backgroundColor: color }}
    />
  );
};

Divider.defaultProps = {
};

export default Divider;
