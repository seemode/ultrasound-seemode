/* eslint-disable react/require-default-props */
import React, { forwardRef } from 'react';
import cnames from 'classnames';
import styles from './styles.module.scss';
import getElementType from '../utils/getElementType';
import IProps from "../interface"

const AXIS_STYLE_PROPS = [
  'marginX',
  'marginY',
  'paddingX',
  'paddingY',
];

const STYLE_PROPS = [
  'display',
  'flexDirection',
  'flexWrap',
  'alignItems',
  'justifyContent',
  'padding',
  'paddingTop',
  'paddingBottom',
  'paddingLeft',
  'paddingRight',
  'margin',
  'marginTop',
  'marginBottom',
  'marginLeft',
  'marginRight',
];

const AXIS_STYLE_REGEX = /(padding|margin)([XY])/;

const processValue = (declaration:any, props:any) => {
  const value = props[declaration];

  if (typeof value !== 'undefined' && value !== null) {
    if (declaration.startsWith('padding') || declaration.startsWith('margin')) {
      return `${value}`.replace('.', '');
    }

    return value;
  }

  return null;
};

const Box :any = forwardRef(({
  className,
  style,
  children,
  flex,
  innerRef,
  ...props
} :IProps , ref)  => {
  let classNames = cnames(styles.box, { [className]: className });

  STYLE_PROPS.forEach((declaration) => {
    const value = processValue(declaration, props);
    if (!value) return;

    classNames = cnames(classNames, styles[`${declaration}-${value}`]);
  });

  const axisSelectors = AXIS_STYLE_PROPS
    .reduce((selectors:any, declaration:any) => {
      const value = processValue(declaration, props);
      if (!value) return selectors;

      // eslint-disable-next-line no-unused-vars
      const [type, axis] = AXIS_STYLE_REGEX.exec(declaration) as any;

      const directions = axis === 'X' ? ['Left', 'Right'] : ['Top', 'Bottom'];

      const newSelectors = directions
        .filter(direction => !classNames.includes(`${type}${direction}`))
        .map(direction => styles[`${type}${direction}-${value}`]);

      return [...selectors, ...newSelectors];
    }, []);

  classNames = cnames(classNames, axisSelectors);

  const restProps = Object.entries(props).reduce((memo, [name, value]) => {
    if ([...STYLE_PROPS, ...AXIS_STYLE_PROPS].includes(name)) return memo;
    if (name === 'element') return memo;

    return { ...memo, [name]: value };
  }, {});

  const Element = getElementType(Box, props);

  return (
    <Element
      ref={ref}
      style={{
        flex,
        ...style,
      }}
      className={classNames}
      {...restProps}
    >
      { children }
    </Element>
  );
});

Box.defaultProps = {
  element: 'div',
  tracking: null,
};

Box.displayName = 'Box';

export default Box;
