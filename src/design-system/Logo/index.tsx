import React from 'react';
import Image from '../Image';
import dark from './img/seeModeLogoDark.svg';
import light from './img/seeModeLogoLight.svg';
import IProps from '../interface';

const Logo = ({ variant, ...props }:IProps)  => {
  return (
    <Image src={variant === 'dark' ? dark : light} {...props} />
  );
};

export default Logo;
