import React, { useMemo } from 'react';
import Box from '../Box';
import IProps from '../interface';

const directions = {
  vertical: 'column',
  horizontal: 'row',
};

const Stack = ({
  children,
  spacing,
  direction,
  /** Box props */
  padding,
  paddingTop,
  paddingBottom,
  paddingRight,
  paddingLeft,
  paddingX,
  paddingY,
  alignItems,
  justifyContent,
}:IProps) => {
  const memoizedChildrenMap = useMemo(() => {
    const stackItemProps = (index:any) => {
      const stackMargin = index === children.length - 1 ? 0 : spacing;
      return (
        direction === 'horizontal' ? {
          marginRight: stackMargin,
          display: 'inline-flex',
        } : {
          marginBottom: stackMargin,
        }
      );
    };

    return React.Children.map(children, (child, index) => (
      <Box {...stackItemProps(index)}>
        {child}
      </Box>
    ));
  }, [children, spacing, direction]);

  return (
    <Box
      display="flex"
      flexDirection={directions[direction]} 
      flexWrap={direction === 'horizontal' ? 'nowrap' : undefined}
      {...({
        padding,
        paddingTop,
        paddingBottom,
        paddingRight,
        paddingLeft,
        paddingX,
        paddingY,
        alignItems,
        justifyContent,
      })}
    >
      {memoizedChildrenMap}
    </Box>
  );
};

Stack.defaultProps = {
  direction: 'vertical',
};

export default Stack;
