import React, { Component } from 'react';
import cnames from 'classnames';
import get from 'lodash/get';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from "../interface"

const RATIO_TYPES = ['square', 'portrait', 'landscape'];

const DEFAULT_PLACEHOLDER = 'data:image/gif;base64,R0lGODdhFQAXAPAAANba3wAAACwAAAAAFQAXAAACFISPqcvtD6OctNqLs968+w+GolUAADs=';

const DEFAULT_OFFSET_TOP_BOUNDARY_BY_PX = 0;

export const generatePlaceholderSrc = ({ width, height}:IProps) => {
  const canvas = document.createElement('canvas');
  const drawingContext :any = canvas.getContext('2d') 
  canvas.width = width;
  canvas.height = height;
  drawingContext.fillStyle  = '#d6dadf';
  drawingContext.fillRect(0, 0, width, height);

  return canvas.toDataURL('image/png');
};

export const getTypeAndMode = (ratio:any) => {
  if (!ratio) return null;

  if (RATIO_TYPES.includes(ratio)) {
    return [ratio, 'cover'];
  }

  if (typeof ratio !== 'object') return null;

  const type = get(ratio, 'type');
  const mode = get(ratio, 'mode', 'cover');

  return type ? [type, mode] : null;
};


const WithRatio = ({
  children,
  size,
  ratio,
  roundedCorners,
} : IProps) => {
  const typeAndMode = getTypeAndMode(ratio);

  if (!typeAndMode) return children;

  const [type, mode] = typeAndMode;

  const ratioClasses = cnames(styles.ratioOuter, styles[mode], styles[type], {
    [styles.rounded]: roundedCorners,
  });

  return (
    <Box className={size && styles[size]}>
      <Box className={ratioClasses}>
        <Box className={styles.ratioInner}>
          { children }
        </Box>
      </Box>
    </Box>
  );
};

WithRatio.defaultProps = {
  ratio: null,
};

const Image = (props:any) => {
    const {
      src,
      size,
      fluid,
      roundedCorners,
      round,
      className, // Exclude from ...rest
      style, // Exclude from ...rest
      srcSet,
      ratio,
      ...rest
    } = props;

    const classNames = cnames(styles.image, {
      [styles[size]]: !!size,
      [styles.rounded]: roundedCorners,
      [styles.fluid]: fluid,
      [styles.round]: round,
    });

    const srcProps = { src };

    if (srcSet !== '') {
      srcProps.src = srcSet;
    }

    return (
      <WithRatio
        ratio={ratio}
        roundedCorners={roundedCorners}
        size={size}
        {...rest}
      >
        <img
          className={classNames}
          {...srcProps}
          {...rest}
        />
      </WithRatio>
    );

}

Image.defaultProps = {
  size: null,
  fluid: false,
  roundedCorners: true,
  round: false,
  ratio: null,
  className: '',
  srcSet: '',
};

export default Image;
