import React from 'react';
import * as Icon from 'react-feather';
import styles from './styles.module.scss';

const SettingsIcon =  (props:any) => <Icon.Settings {...props} className={styles.icon} />
const UserIcon =  (props:any) => <Icon.User {...props} className={styles.icon} />
const InboxIcon =  (props:any) => <Icon.Inbox {...props} className={styles.icon} />
const ClockIcon =  (props:any) => <Icon.Clock {...props} className={styles.icon} />
const UploadIcon =  (props:any) => <Icon.Upload {...props} className={styles.icon} />
const ArrowLeftIcon =  (props:any) => <Icon.ArrowLeft {...props} className={styles.icon} />
const ListIcon =  (props:any) => <Icon.List {...props} className={styles.icon} />
const GridIcon =  (props:any) => <Icon.Grid {...props} className={styles.icon} />
const LayersIcon =  (props:any) => <Icon.Layers {...props} className={styles.icon} />
const SquareIcon =  (props:any) => <Icon.Square {...props} className={styles.icon} />
const ZoomInIcon =  (props:any) => <Icon.ZoomIn {...props} className={styles.icon} />
const ZoomOutIcon =  (prop:any) => <Icon.ZoomOut {...props} className={styles.icon} />
const PlusIcon =  (props:any) => <Icon.Plus {...props} className={styles.icon} />
const SortIcon = (props:any) => (
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={styles.icon}>
    <path fillRule="evenodd" clipRule="evenodd" d="M12 4C12.5523 4 13 4.44772 13 5V19C13 19.5523 12.5523 20 12 20C11.4477 20 11 19.5523 11 19V5C11 4.44772 11.4477 4 12 4Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M9 12C9 12.5523 8.55228 13 8 13L4 13C3.44772 13 3 12.5523 3 12C3 11.4477 3.44772 11 4 11L8 11C8.55228 11 9 11.4477 9 12Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M15 12C15 12.5523 15.4477 13 16 13L20 13C20.5523 13 21 12.5523 21 12C21 11.4477 20.5523 11 20 11L16 11C15.4477 11 15 11.4477 15 12Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M7.29289 14.2929C7.68342 13.9024 8.31658 13.9024 8.70711 14.2929L12 17.5858L15.2929 14.2929C15.6834 13.9024 16.3166 13.9024 16.7071 14.2929C17.0976 14.6834 17.0976 15.3166 16.7071 15.7071L12.7071 19.7071C12.3166 20.0976 11.6834 20.0976 11.2929 19.7071L7.29289 15.7071C6.90237 15.3166 6.90237 14.6834 7.29289 14.2929Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M16.7071 9.70711C16.3166 10.0976 15.6834 10.0976 15.2929 9.70711L12 6.41421L8.70711 9.70711C8.31658 10.0976 7.68342 10.0976 7.29289 9.70711C6.90237 9.31658 6.90237 8.68342 7.29289 8.29289L11.2929 4.29289C11.6834 3.90237 12.3166 3.90237 12.7071 4.29289L16.7071 8.29289C17.0976 8.68342 17.0976 9.31658 16.7071 9.70711Z" fill="currentColor"/>
  </svg>
)
const FilterIcon = (props:any) => (
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={styles.icon}>
    <path fillRule="evenodd" clipRule="evenodd" d="M4 10C4 9.44772 4.44772 9 5 9H19C19.5523 9 20 9.44772 20 10C20 10.5523 19.5523 11 19 11H5C4.44772 11 4 10.5523 4 10Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M8 18C8 17.4477 8.44772 17 9 17H15C15.5523 17 16 17.4477 16 18C16 18.5523 15.5523 19 15 19H9C8.44772 19 8 18.5523 8 18Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M6 14C6 13.4477 6.44772 13 7 13H17C17.5523 13 18 13.4477 18 14C18 14.5523 17.5523 15 17 15H7C6.44772 15 6 14.5523 6 14Z" fill="currentColor"/>
    <path fillRule="evenodd" clipRule="evenodd" d="M6 6C6 5.44772 6.44772 5 7 5H17C17.5523 5 18 5.44772 18 6C18 6.55228 17.5523 7 17 7H7C6.44772 7 6 6.55228 6 6Z" fill="currentColor"/>
  </svg>
)

export {
  SettingsIcon,
  UserIcon,
  InboxIcon,
  ClockIcon,
  UploadIcon,
  SortIcon,
  FilterIcon,
  ArrowLeftIcon,
  ListIcon,
  GridIcon,
  LayersIcon,
  SquareIcon,
  ZoomInIcon,
  ZoomOutIcon,
  PlusIcon,
};
