import React from 'react';
import cnames from 'classnames';
import Text from '../Text';
import styles from './styles.module.scss';
import IProps from "../interface"

const Badge = ({ children, intent, subtle, styles }:IProps) => {
  if (!children) return null;
  return (
    <div
      className={
        cnames(
          styles.badge,
          styles[intent],
          {
            [styles.subtle]: subtle,
          },
        )
      }
      title={children}
    >
      <Text type="caption">
        <span className={styles.badgeText}>{ children }</span>
      </Text>
    </div>
  );
};

Badge.defaultProps = {
  subtle: false,
  intent: 'neutral',
  styles,
};

export default Badge;
