import React, { forwardRef } from 'react';
import cnames from 'classnames';
import omit from 'lodash/omit';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from "../interface"

const validDisplayTypes = ['block', 'inline-block', 'inline'];

const Text = forwardRef((props:any, ref:any) => {
  const {
    // eslint-disable-next-line max-len
    children, type, loading, muted, className, styles, element, block, lineClamp: textLineClamp, // Text props
    // eslint-disable-next-line max-len
    margin, marginTop, marginLeft, marginRight, marginBottom, marginX, marginY, display, // Box props
    ...rest
  } = props;

  // validate lineClamp
  // handle it being a string (prop types warning)
  let lineClamp = parseInt(textLineClamp, 10);
  if (lineClamp < 0) {
    // eslint-disable-next-line no-console
    console.warn('LineClamp cannot have a negative value, the absolute value will be used instead.');
    lineClamp = Math.abs(lineClamp);
  }

  const isLineWrap = lineClamp && lineClamp === 1;
  const isLineClamp = !isLineWrap && !!lineClamp;

  // filter out Box props we don't need
  const remainingTextProps = omit(rest, Box.propNames);

  // handle applicable display types
  let displayType = (
    // override any passed in display prop if lineClamp is present
    (lineClamp && (lineClamp > 1 ? '-webkit-box' : 'block'))
    || (block && 'block')
    || display
  );
  if (!validDisplayTypes.includes(displayType)) displayType = undefined;

  const classNames = cnames(styles.text, {
    [styles[type]]: !!type,
    [styles.muted]: muted,
    [styles.lineClamp]: isLineClamp,
    [styles.nowrap]: isLineWrap,
    [className]: className,
  });

  const sharedProps = {
    display: displayType,
    className: classNames,
    element,
    margin,
    marginTop,
    marginLeft,
    marginRight,
    marginBottom,
    marginX,
    marginY,
    ...remainingTextProps,
  };

  // Handle lineClamp, if it's 1 line, use nowrap instead
  const clampStyle = isLineClamp ? { WebkitLineClamp: lineClamp } : undefined;

  if (!loading) {
    return (
      <Box
        {...sharedProps}
        style={clampStyle}
        ref={ref}
      >
        { children }
      </Box>
    );
  }

  const { dangerouslySetInnerHTML, ...loadingRest } = sharedProps as any;

  return (
    <Box
      {...loadingRest}
      style={clampStyle}
      ref={ref}
    >
      <span
        className={styles.loading}
        {...({ dangerouslySetInnerHTML, children })}
      />
    </Box>
  );
});

Text.defaultProps = {
  className: '',
  element: 'span',
  type: 'body',
  loading: false,
  muted: false,
  styles,
};

Text.displayName = 'Text';

export default Text;
