import React, { forwardRef } from 'react';
import cnames from 'classnames';
import Box from '../Box';
import styles from './styles.module.scss';
import IProps from '../interface';
const Card = forwardRef(({
  children,
  elevation,
  roundedCorners,
  fluid,
  transparent,
  className, // Exclude from ...rest
  style, // Exclude from ...rest
  ...rest
} : any, ref) => {
  const classNames = cnames(styles.card, {
    [styles.rounded]: roundedCorners,
    [styles.fluid]: fluid,
    [styles.transparent]: transparent,
    [styles.elevationLow]: elevation === 'low',
    [styles.elevationMedium]: elevation === 'medium',
    [styles.elevationHigh]: elevation === 'high',
  });

  return (
    <Box {...rest} className={classNames} ref={ref}>{children}</Box>
  );
});

Card.defaultProps = {
  className: '',
  elevation: null,
  roundedCorners: true,
  fluid: false,
  transparent: false,
  padding: 'm',
} as any;

export default Card;
